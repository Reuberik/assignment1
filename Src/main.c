/**
 ******************************************************************************
 * File Name          : main.c
 * Description        : Main program body
 ******************************************************************************
 *
 * COPYRIGHT(c) 2017 STMicroelectronics
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *   3. Neither the name of STMicroelectronics nor the names of its contributors
 *      may be used to endorse or promote products derived from this software
 *      without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************
 */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f4xx_hal.h"

/* USER CODE BEGIN Includes */

/*****************************************************************************
 *****************************************************************************
 * Project Name		Assignment1				                                *
 * Author:          Erik Jonsson                                            *
 * Run with:     	TrueSTUDIO for ARM 7.0.1		                        *
 * Date:            2017-01-29						                        *
 * Description:     Read keys from K6L module and then send value to 		*
 * 					display as hexa and decimal.							*
 *****************************************************************************
 *****************************************************************************/

/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/
UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

#define STB(x)		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, x)			//A3
#define CLK(x)		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_1, x)			//A4
#define DIO(x)		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_0, x)			//A5
#define rDIO		HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_0)				//A5
#define LD2(x)		HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, x); 	//LD2
#define HIGH		GPIO_PIN_SET
#define LOW			GPIO_PIN_RESET
#define CDELAY		cdelay();

#define DATA		0x40
#define TESTMODE	0x08
#define FIXED		0x04
#define READ		0x02

#define DISPLAY		0x80	//+ intensity
#define	ON			0x08

#define ADDRESS		0xc0	//+ address


/* byte that lights segments of characters on the 7 segment display */
#define SEG7_0		0b00111111
#define SEG7_1		0b00000110
#define SEG7_2		0b01011011
#define SEG7_3		0b01001111
#define SEG7_4		0b01100110
#define SEG7_5		0b01101101
#define SEG7_6		0b01111101
#define SEG7_7		0b00000111
#define SEG7_8		0b01111111
#define SEG7_9		0b01101111
#define SEG7_A		0b01110111
#define	SEG7_B		0b01111100
#define SEG7_C		0b00111001
#define SEG7_D		0b01011110
#define SEG7_E		0b01111001
#define SEG7_F		0b01110001



uint8_t hex_display[2];				// Array to store hexadecimal digits, (yeah, global variable might not be the best choice, but in this instance I chose to be lazy)
uint8_t dec_display[3];				// See comment above but for decimal digits

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void Error_Handler(void);
static void MX_GPIO_Init(void);
static void MX_USART2_UART_Init(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

void SystemClock_Config(void);
static void MX_GPIO_Init(void);
void sendbyte(uint8_t data);
void udelay(volatile unsigned int delay);
void cdelay(void);
uint8_t decode(uint32_t);
uint32_t recieveint32(void);							// Read from TM1638
void send_to_display(uint8_t address, uint8_t value);	// Simplifies sending byte to memory on TM1638 with a specific address
void get_hexdigits(uint8_t);							// Break byte into individual hexadecimal digits, (character-values i.e. '1' or 'A')
void get_decimal(uint8_t);								// Break byte into individual decimal digits, (character-values i.e. '2' or '9')
void hex_to_display(void);								// Sends value decoded as hexa for 7 segment to the display & LEDs
void dec_to_display(void);								// Sends value decoded as decimal for 7 segment to the display
/* USER CODE END PFP */

/* USER CODE BEGIN 0 */


/* USER CODE END 0 */

int main(void)
{

	/* USER CODE BEGIN 1 */
	uint32_t recieved_bytes;			// Store 32bit value received from TM1638
	uint8_t decoded_byte = 0;			// Store 8bit decoded value from TM1638

	/* USER CODE END 1 */

	/* MCU Configuration----------------------------------------------------------*/

	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();

	/* Configure the system clock */
	SystemClock_Config();

	/* Initialize all configured peripherals */
	MX_GPIO_Init();
	MX_USART2_UART_Init();

	/* USER CODE BEGIN 2 */

	STB(HIGH);
	DIO(HIGH);
	CLK(HIGH);
	CDELAY;

	STB(LOW);						// Activate slave select(strobe)
	CDELAY;							// Delay to be sure to value will be received properly
	sendbyte(DISPLAY | 0x0A);		// Call function "sendbyte" with instruction to activate the display and set pulse width to 4/16
	STB(HIGH);						// Deactivate strobe
	CDELAY;							// Delay to make sure next instruction is received properly

	STB(LOW);						// Start sending
	CDELAY;							// Delay to be sure to value will be received properly
	sendbyte(DATA);					// Send instruction to auto increment memory address
	for(int i = 16; i > 0; i--)
	{
		sendbyte(0x00);				// Send data to turn all display segments and LEDs off
	}
	STB(HIGH);						// Done sending
	CDELAY;							// Delay to make sure next instruction is received properly

	/* USER CODE END 2 */

	/* Infinite loop */
	/* USER CODE BEGIN WHILE */
	while (1)
	{
		/* USER CODE END WHILE */

		/* USER CODE BEGIN 3 */

		recieved_bytes = recieveint32();			// Receive int32 from TM1638
		decoded_byte = decode(recieved_bytes);		// Decode said int32 into byte with bits rearranged for correct value
		get_hexdigits(decoded_byte);				// Break decoded byte into hexadecimal digits (store in global variable 'hex_digits[0&1]')
		get_decimal(decoded_byte);					// Break decoded byte into hexadecimal digits (store in global variable 'dec_digits[0,1&2]')
		hex_to_display();							// Print hex digits to display
		dec_to_display();							// Print decimal digits to display

	}
	/* USER CODE END 3 */

}

/** System Clock Configuration
 */
void SystemClock_Config(void)
{

	RCC_OscInitTypeDef RCC_OscInitStruct;
	RCC_ClkInitTypeDef RCC_ClkInitStruct;

	/**Configure the main internal regulator output voltage
	 */
	__HAL_RCC_PWR_CLK_ENABLE();

	__HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

	/**Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
	RCC_OscInitStruct.HSIState = RCC_HSI_ON;
	RCC_OscInitStruct.HSICalibrationValue = 16;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
	RCC_OscInitStruct.PLL.PLLM = 8;
	RCC_OscInitStruct.PLL.PLLN = 100;
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
	RCC_OscInitStruct.PLL.PLLQ = 4;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	{
		Error_Handler();
	}

	/**Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
			|RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_3) != HAL_OK)
	{
		Error_Handler();
	}

	/**Configure the Systick interrupt time
	 */
	HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

	/**Configure the Systick
	 */
	HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

	/* SysTick_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USART2 init function */
static void MX_USART2_UART_Init(void)
{

	huart2.Instance = USART2;
	huart2.Init.BaudRate = 115200;
	huart2.Init.WordLength = UART_WORDLENGTH_8B;
	huart2.Init.StopBits = UART_STOPBITS_1;
	huart2.Init.Parity = UART_PARITY_NONE;
	huart2.Init.Mode = UART_MODE_TX_RX;
	huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	huart2.Init.OverSampling = UART_OVERSAMPLING_16;
	if (HAL_UART_Init(&huart2) != HAL_OK)
	{
		Error_Handler();
	}

}

/** Configure pins as 
 * Analog
 * Input
 * Output
 * EVENT_OUT
 * EXTI
 */
static void MX_GPIO_Init(void)
{

	GPIO_InitTypeDef GPIO_InitStruct;

	/* GPIO Ports Clock Enable */
	__HAL_RCC_GPIOC_CLK_ENABLE();
	__HAL_RCC_GPIOH_CLK_ENABLE();
	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOB_CLK_ENABLE();

	/*Configure GPIO pin : B1_Pin */
	GPIO_InitStruct.Pin = B1_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_EVT_RISING;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(B1_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : DIO_Pin */
	GPIO_InitStruct.Pin = DIO_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(DIO_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : CLK_Pin */
	GPIO_InitStruct.Pin = CLK_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(CLK_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : LD2_Pin */
	GPIO_InitStruct.Pin = LD2_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(LD2_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : STB_Pin */
	GPIO_InitStruct.Pin = STB_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(STB_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOC, DIO_Pin|CLK_Pin, GPIO_PIN_RESET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_RESET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(STB_GPIO_Port, STB_Pin, GPIO_PIN_RESET);

}

/* USER CODE BEGIN 4 */

void cdelay(void)
{
	volatile int delay;
	for (delay=8;delay!=0;delay--);
}

void udelay(volatile unsigned int delay)
{
	delay*=11.206;
	for (;delay!=0;delay--);
}

void sendbyte(uint8_t data)
{
	uint8_t i;

	for(i=0x1;i!=0;i<<=1)
	{
		DIO(!!(data & i));
		CLK(LOW);
		CDELAY;
		CLK(HIGH);
		CDELAY;
	}
}

uint8_t decode(uint32_t value)
{
	return
			(value & 0x00000001)<<7  |
			(value & 0x00000100)>>2  |
			(value & 0x00010000)>>11 |
			(value & 0x01000000)>>20 |
			(value & 0x00000010)>>1  |
			(value & 0x00001000)>>10 |
			(value & 0x00100000)>>19 |
			(value & 0x10000000)>>28;
}


/* Eriks functions start */


/* Function to send decimal digits to display */
void dec_to_display(void)
{
	switch (dec_display[2])
	{
	case '0':
		send_to_display((ADDRESS | 0x06), SEG7_0);		// Call function send_to_display with the address for GRID4 and the -
		break;											// - byte needed to print a '0' on the display
	case '1':
		send_to_display((ADDRESS | 0x06), SEG7_1);		// -"- for rest of switch
		break;
	case '2':
		send_to_display((ADDRESS | 0x06), SEG7_2);
		break;
	case '3':
		send_to_display((ADDRESS | 0x06), SEG7_3);
		break;
	case '4':
		send_to_display((ADDRESS | 0x06), SEG7_4);
		break;
	case '5':
		send_to_display((ADDRESS | 0x06), SEG7_5);
		break;
	case '6':
		send_to_display((ADDRESS | 0x06), SEG7_6);
		break;
	case '7':
		send_to_display((ADDRESS | 0x06), SEG7_7);
		break;
	case '8':
		send_to_display((ADDRESS | 0x06), SEG7_8);
		break;
	case '9':
		send_to_display((ADDRESS | 0x06), SEG7_9);
		break;
	}


	switch (dec_display[1])
	{
	case '0':
		send_to_display((ADDRESS | 0x04), SEG7_0);		// See comments for previous switch, replace address with GRID 3
		break;
	case '1':
		send_to_display((ADDRESS | 0x04), SEG7_1);
		break;
	case '2':
		send_to_display((ADDRESS | 0x04), SEG7_2);
		break;
	case '3':
		send_to_display((ADDRESS | 0x04), SEG7_3);
		break;
	case '4':
		send_to_display((ADDRESS | 0x04), SEG7_4);
		break;
	case '5':
		send_to_display((ADDRESS | 0x04), SEG7_5);
		break;
	case '6':
		send_to_display((ADDRESS | 0x04), SEG7_6);
		break;
	case '7':
		send_to_display((ADDRESS | 0x04), SEG7_7);
		break;
	case '8':
		send_to_display((ADDRESS | 0x04), SEG7_8);
		break;
	case '9':
		send_to_display((ADDRESS | 0x04), SEG7_9);
		break;
	}

	switch (dec_display[0])
	{
	case '0':
		send_to_display((ADDRESS | 0x02), SEG7_0);		// See comments for previous switch, replace address with GRID 2
		break;
	case '1':
		send_to_display((ADDRESS | 0x02), SEG7_1);
		break;
	case '2':
		send_to_display((ADDRESS | 0x02), SEG7_2);
		break;
	case '3':
		send_to_display((ADDRESS | 0x02), SEG7_3);
		break;
	case '4':
		send_to_display((ADDRESS | 0x02), SEG7_4);
		break;
	case '5':
		send_to_display((ADDRESS | 0x02), SEG7_5);
		break;
	case '6':
		send_to_display((ADDRESS | 0x02), SEG7_6);
		break;
	case '7':
		send_to_display((ADDRESS | 0x02), SEG7_7);
		break;
	case '8':
		send_to_display((ADDRESS | 0x02), SEG7_8);
		break;
	case '9':
		send_to_display((ADDRESS | 0x02), SEG7_9);
		break;
	}
}


/* Function to send hexadecimal digits to display */
void hex_to_display(void)
{
	switch (hex_display[1])
	{
	case '0':
		send_to_display((ADDRESS | 0x0E), SEG7_0);		// Call function send_to_display with the address for GRID8 and the -
		send_to_display((ADDRESS | 0x09), 0);			// - byte needed to print a '0' on the display and also bytes to light -
		send_to_display((ADDRESS | 0x0B), 0);			// - the corresponding LEDs
		send_to_display((ADDRESS | 0x0D), 0);
		send_to_display((ADDRESS | 0x0F), 0);
		break;
	case '1':
		send_to_display((ADDRESS | 0x0E), SEG7_1);		// -"- for rest of switch
		send_to_display((ADDRESS | 0x09), 0);
		send_to_display((ADDRESS | 0x0B), 0);
		send_to_display((ADDRESS | 0x0D), 0);
		send_to_display((ADDRESS | 0x0F), 1);

		break;
	case '2':
		send_to_display((ADDRESS | 0x0E), SEG7_2);
		send_to_display((ADDRESS | 0x09), 0);
		send_to_display((ADDRESS | 0x0B), 0);
		send_to_display((ADDRESS | 0x0D), 1);
		send_to_display((ADDRESS | 0x0F), 0);
		break;
	case '3':
		send_to_display((ADDRESS | 0x0E), SEG7_3);
		send_to_display((ADDRESS | 0x09), 0);
		send_to_display((ADDRESS | 0x0B), 0);
		send_to_display((ADDRESS | 0x0D), 1);
		send_to_display((ADDRESS | 0x0F), 1);
		break;
	case '4':
		send_to_display((ADDRESS | 0x0E), SEG7_4);
		send_to_display((ADDRESS | 0x09), 0);
		send_to_display((ADDRESS | 0x0B), 1);
		send_to_display((ADDRESS | 0x0D), 0);
		send_to_display((ADDRESS | 0x0F), 0);
		break;
	case '5':
		send_to_display((ADDRESS | 0x0E), SEG7_5);
		send_to_display((ADDRESS | 0x09), 0);
		send_to_display((ADDRESS | 0x0B), 1);
		send_to_display((ADDRESS | 0x0D), 0);
		send_to_display((ADDRESS | 0x0F), 1);
		break;
	case '6':
		send_to_display((ADDRESS | 0x0E), SEG7_6);
		send_to_display((ADDRESS | 0x09), 0);
		send_to_display((ADDRESS | 0x0B), 1);
		send_to_display((ADDRESS | 0x0D), 1);
		send_to_display((ADDRESS | 0x0F), 0);
		break;
	case '7':
		send_to_display((ADDRESS | 0x0E), SEG7_7);
		send_to_display((ADDRESS | 0x09), 0);
		send_to_display((ADDRESS | 0x0B), 1);
		send_to_display((ADDRESS | 0x0D), 1);
		send_to_display((ADDRESS | 0x0F), 1);
		break;
	case '8':
		send_to_display((ADDRESS | 0x0E), SEG7_8);
		send_to_display((ADDRESS | 0x09), 1);
		send_to_display((ADDRESS | 0x0B), 0);
		send_to_display((ADDRESS | 0x0D), 0);
		send_to_display((ADDRESS | 0x0F), 0);
		break;
	case '9':
		send_to_display((ADDRESS | 0x0E), SEG7_9);
		send_to_display((ADDRESS | 0x09), 1);
		send_to_display((ADDRESS | 0x0B), 0);
		send_to_display((ADDRESS | 0x0D), 0);
		send_to_display((ADDRESS | 0x0F), 1);
		break;
	case 'A':
		send_to_display((ADDRESS | 0x0E), SEG7_A);
		send_to_display((ADDRESS | 0x09), 1);
		send_to_display((ADDRESS | 0x0B), 0);
		send_to_display((ADDRESS | 0x0D), 1);
		send_to_display((ADDRESS | 0x0F), 0);
		break;
	case 'B':
		send_to_display((ADDRESS | 0x0E), SEG7_B);
		send_to_display((ADDRESS | 0x09), 1);
		send_to_display((ADDRESS | 0x0B), 0);
		send_to_display((ADDRESS | 0x0D), 1);
		send_to_display((ADDRESS | 0x0F), 1);
		break;
	case 'C':
		send_to_display((ADDRESS | 0x0E), SEG7_C);
		send_to_display((ADDRESS | 0x09), 1);
		send_to_display((ADDRESS | 0x0B), 1);
		send_to_display((ADDRESS | 0x0D), 0);
		send_to_display((ADDRESS | 0x0F), 0);
		break;
	case 'D':
		send_to_display((ADDRESS | 0x0E), SEG7_D);
		send_to_display((ADDRESS | 0x09), 1);
		send_to_display((ADDRESS | 0x0B), 1);
		send_to_display((ADDRESS | 0x0D), 0);
		send_to_display((ADDRESS | 0x0F), 1);
		break;
	case 'E':
		send_to_display((ADDRESS | 0x0E), SEG7_E);
		send_to_display((ADDRESS | 0x09), 1);
		send_to_display((ADDRESS | 0x0B), 1);
		send_to_display((ADDRESS | 0x0D), 1);
		send_to_display((ADDRESS | 0x0F), 0);
		break;
	case 'F':
		send_to_display((ADDRESS | 0x0E), SEG7_F);
		send_to_display((ADDRESS | 0x09), 1);
		send_to_display((ADDRESS | 0x0B), 1);
		send_to_display((ADDRESS | 0x0D), 1);
		send_to_display((ADDRESS | 0x0F), 1);
		break;
	}


	switch (hex_display[0])
	{
	case '0':
		send_to_display((ADDRESS | 0x0C), SEG7_0);		// See comments for previous switch, replace address with GRID 7
		send_to_display((ADDRESS | 0x01), 0);
		send_to_display((ADDRESS | 0x03), 0);
		send_to_display((ADDRESS | 0x05), 0);
		send_to_display((ADDRESS | 0x07), 0);
		break;
	case '1':
		send_to_display((ADDRESS | 0x0C), SEG7_1);
		send_to_display((ADDRESS | 0x01), 0);
		send_to_display((ADDRESS | 0x03), 0);
		send_to_display((ADDRESS | 0x05), 0);
		send_to_display((ADDRESS | 0x07), 1);
		break;
	case '2':

		send_to_display((ADDRESS | 0x0C), SEG7_2);
		send_to_display((ADDRESS | 0x01), 0);
		send_to_display((ADDRESS | 0x03), 0);
		send_to_display((ADDRESS | 0x05), 1);
		send_to_display((ADDRESS | 0x07), 0);
		break;
	case '3':
		send_to_display((ADDRESS | 0x0C), SEG7_3);
		send_to_display((ADDRESS | 0x01), 0);
		send_to_display((ADDRESS | 0x03), 0);
		send_to_display((ADDRESS | 0x05), 1);
		send_to_display((ADDRESS | 0x07), 1);
		break;
	case '4':
		send_to_display((ADDRESS | 0x0C), SEG7_4);
		send_to_display((ADDRESS | 0x01), 0);
		send_to_display((ADDRESS | 0x03), 1);
		send_to_display((ADDRESS | 0x05), 0);
		send_to_display((ADDRESS | 0x07), 0);
		break;
	case '5':
		send_to_display((ADDRESS | 0x0C), SEG7_5);
		send_to_display((ADDRESS | 0x01), 0);
		send_to_display((ADDRESS | 0x03), 1);
		send_to_display((ADDRESS | 0x05), 0);
		send_to_display((ADDRESS | 0x07), 1);
		break;
	case '6':
		send_to_display((ADDRESS | 0x0C), SEG7_6);
		send_to_display((ADDRESS | 0x01), 0);
		send_to_display((ADDRESS | 0x03), 1);
		send_to_display((ADDRESS | 0x05), 1);
		send_to_display((ADDRESS | 0x07), 0);
		break;
	case '7':
		send_to_display((ADDRESS | 0x0C), SEG7_7);
		send_to_display((ADDRESS | 0x01), 0);
		send_to_display((ADDRESS | 0x03), 1);
		send_to_display((ADDRESS | 0x05), 1);
		send_to_display((ADDRESS | 0x07), 1);
		break;
	case '8':
		send_to_display((ADDRESS | 0x0C), SEG7_8);
		send_to_display((ADDRESS | 0x01), 1);
		send_to_display((ADDRESS | 0x03), 0);
		send_to_display((ADDRESS | 0x05), 0);
		send_to_display((ADDRESS | 0x07), 0);
		break;
	case '9':
		send_to_display((ADDRESS | 0x0C), SEG7_9);
		send_to_display((ADDRESS | 0x01), 1);
		send_to_display((ADDRESS | 0x03), 0);
		send_to_display((ADDRESS | 0x05), 0);
		send_to_display((ADDRESS | 0x07), 1);
		break;
	case 'A':
		send_to_display((ADDRESS | 0x0C), SEG7_A);
		send_to_display((ADDRESS | 0x01), 1);
		send_to_display((ADDRESS | 0x03), 0);
		send_to_display((ADDRESS | 0x05), 1);
		send_to_display((ADDRESS | 0x07), 0);
		break;
	case 'B':
		send_to_display((ADDRESS | 0x0C), SEG7_B);
		send_to_display((ADDRESS | 0x01), 1);
		send_to_display((ADDRESS | 0x03), 0);
		send_to_display((ADDRESS | 0x05), 1);
		send_to_display((ADDRESS | 0x07), 1);
		break;
	case 'C':
		send_to_display((ADDRESS | 0x0C), SEG7_C);
		send_to_display((ADDRESS | 0x01), 1);
		send_to_display((ADDRESS | 0x03), 1);
		send_to_display((ADDRESS | 0x05), 0);
		send_to_display((ADDRESS | 0x07), 0);
		break;
	case 'D':
		send_to_display((ADDRESS | 0x0C), SEG7_D);
		send_to_display((ADDRESS | 0x01), 1);
		send_to_display((ADDRESS | 0x03), 1);
		send_to_display((ADDRESS | 0x05), 0);
		send_to_display((ADDRESS | 0x07), 1);
		break;
	case 'E':
		send_to_display((ADDRESS | 0x0C), SEG7_E);
		send_to_display((ADDRESS | 0x01), 1);
		send_to_display((ADDRESS | 0x03), 1);
		send_to_display((ADDRESS | 0x05), 1);
		send_to_display((ADDRESS | 0x07), 0);
		break;
	case 'F':
		send_to_display((ADDRESS | 0x0C), SEG7_F);
		send_to_display((ADDRESS | 0x01), 1);
		send_to_display((ADDRESS | 0x03), 1);
		send_to_display((ADDRESS | 0x05), 1);
		send_to_display((ADDRESS | 0x07), 1);
		break;
	}
}


/* Break byte into individual hexadecimal digits */
void get_hexdigits(uint8_t hex)
{
	uint8_t temp;			// foo var
	temp = hex/16 + 0x30;	// Get char for '0'-'9'
	if(temp > 0x39)
		temp += 7;			// If 'A'-'F' add 7 to get the correct character
	hex_display[0] = temp;	// Store in global variable, (MSB)

	temp = hex % 16 + 0x30;	// Same as previous but for LSB
	if(temp > 0x39)
		temp += 7;
	hex_display[1] = temp;
}


/* Break byte into individual decimal digits */
void get_decimal(uint8_t dec)
{
	dec_display[0] = dec / 100 + 0x30;		// Get char for MSB
	dec %= 100;
	dec_display[1] = dec / 10 + 0x30;		// Get char for middle bit
	dec_display[2] = dec % 10 + 0x30;		// Get char for LSB
}


/* Function to read from TM1638 */
uint32_t recieveint32(void)
{
	uint32_t i;					// Foo var
	uint32_t data = 0;			// Store received bits

	STB(LOW);					// Start transaction
	CDELAY;						// Wait to make sure STB is stabilized before next instruction
	sendbyte(DATA | READ);		// Send command telling TM1638 you want to read from it
	DIO(HIGH);					// Set DIO to high so you get the pull up
	for(i = 0; i < 32; i++)		// for bytes will be sent, hence 32 iterations
	{
		CLK(LOW);				// Set clock to low
		CDELAY;					// Wait so that the signal has time to stabilize
		data |= rDIO << i;		// Read rDIO and add it's value to the correct bit in data
		CLK(HIGH);				// Clock to high
		CDELAY;					// Wait so that the signal has time to stabilize
	}
	STB(HIGH);					// Transaction finished
	CDELAY;						// Wait to make sure STB is stabilized before next instruction

	return data;
}


/* Function to send a specified byte to a specified address in the display register */
void send_to_display(uint8_t address, uint8_t value)
{
	STB(LOW);				// Start sending
	CDELAY;					// Delay to be sure to value will be received properly
	sendbyte(address);		// Send instruction to write data to specified address in the display register
	sendbyte(value);		// Send data which will light segment 8 on the display, (comma)
	STB(HIGH);				// Done sending
	CDELAY;					// Delay to make sure next instruction is received properly
}

/* Eriks functions end */

/* USER CODE END 4 */

/**
 * @brief  This function is executed in case of error occurrence.
 * @param  None
 * @retval None
 */
void Error_Handler(void)
{
	/* USER CODE BEGIN Error_Handler */
	/* User can add his own implementation to report the HAL error return state */
	while(1)
	{
	}
	/* USER CODE END Error_Handler */
}

#ifdef USE_FULL_ASSERT

/**
 * @brief Reports the name of the source file and the source line number
 * where the assert_param error has occurred.
 * @param file: pointer to the source file name
 * @param line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t* file, uint32_t line)
{
	/* USER CODE BEGIN 6 */
	/* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
	/* USER CODE END 6 */

}

#endif

/**
 * @}
 */

/**
 * @}
 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
